# express-authentication

## installation

```sh
# install packages
npm install #or yarn
# create config
cp config/config.example.js config/config.js
# setup your database details in the config/config.js
# run server
npm start
#run server with nodemon
npm run dev
```
