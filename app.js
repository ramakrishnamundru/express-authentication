const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const passport = require('passport');
const bodyParser = require('body-parser');
const config = require('./config/config');
const app = express();

mongoose.Promise = require('bluebird');
mongoose.connect(config.mongoDbPath || 'mongodb://localhost/express-authentication');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  let allowedOrigins = ['http://localhost'];
  let origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Oriign', origin);
  }
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(passport.initialize());

app.use('/', require('./routes'));

const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');
app.set('port', port);

app.listen(port, () => console.log(`Server listening on port ${port}`));
